import pandas as pd
from os import walk
from numpy import nan

mypath = '/home/chinmay/Desktop/emailBot/auxFunctions/topTen/data'
fileNames = []
for (dirpath, dirnames, filenames) in walk(mypath):
    fileNames.extend(filenames)
    break

for f in fileNames:
    xlxb = pd.ExcelFile('data/'+f, engine='pyxlsb')
    df = pd.read_excel(xlxb, 'Raw data')
    print(df.columns.values)
    print(df['Updated Values'].unique())
    df = df.loc[df['Updated Values'].isin([nan])]
    print(len(df['Interaction ID']))

    with open('icsv_raw_data.txt', 'w') as icsv:
        for ele in df['Interaction ID']:
            ele = str(ele).replace('-','')
            if ele[:2] == '00':
                ele = ele[2:]
            icsv.write(ele+',')

    df = pd.read_excel(xlxb, 'Audit raw')
    # print(df.columns.values)
    # print(df['Updated Values'].unique())
    df = df.loc[df['Tagging'].isin(['Correct'])]
    print(len(df['Interaction ID']))

    with open('icsv_audit_raw.txt', 'w') as icsv:
        for ele in df['Interaction ID']:
            ele = str(ele).replace('-','')
            if ele[:2] == '00':
                ele = ele[2:]
            icsv.write(ele+',')
        