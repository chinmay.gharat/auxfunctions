import pandas as pd

with open('icsv_audit_raw.txt', 'r') as f:
    iidStr = f.read()

iidList = iidStr.split(',')
df = pd.DataFrame({'iids':iidList[:-1]})
df.to_csv('auditIdds.csv', index=False)