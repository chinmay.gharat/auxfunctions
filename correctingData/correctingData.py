import pandas as pd
from json import loads
import re

# p = re.compile('(?<!\\\\)\'')
def correctData(ip):
    try:
        ip = ip['Processing_Variables']        
        # ip = p.sub('\"', ip)
        ip = loads(ip)
        ip = ip['Processed_Email_Body']
        print(ip)
    except:
        pass
    return ip

df = pd.read_csv('data_raw_mo.csv')
print(df.columns.values)
ndf = pd.DataFrame()
ndf['Category'] = df['MasterDept'] + '||' + df['Dept'] + '||' + df['QueryType'] + '||' + df['SubQueryType'] 
ndf['Query'] = df.apply(correctData, axis=1)
ndf['Interaction_Id'] = df.apply(correctData, axis=1)

ndf.to_csv('correctedData.csv',index=False)