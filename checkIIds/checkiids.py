import pandas as pd

df = pd.read_csv('sqlIidPullAudit.csv')
df = df.fillna(0)
df['iid'] = df['iid'].astype('int32')
df['nGlobalCaseID'] = df['nGlobalCaseID'].astype('int32')
iid = list(set(df['iid']))
iid = iid[1:]
nGlobalCaseID = list(df['nGlobalCaseID'])
isPresent = []
isNotPresent = []
for index,i in enumerate(iid):
    print(index+1)
    if i in nGlobalCaseID and i!=0:
        isPresent.append(i)
    else:
        isNotPresent.append(i)
print(f'Present:{len(isPresent)}')
df['presentIids'] = pd.Series(isPresent)
df['presentIids'] = df['presentIids'].fillna(0)
df['presentIids'] = df['presentIids'].astype('int32')
print(f'not present:{len(isNotPresent)}')
df['notPresentIids'] = pd.Series(isNotPresent)
df['notPresentIids'] = df['notPresentIids'].fillna(0)
df['notPresentIids'] = df['notPresentIids'].astype('int32')
print(df.head())
print(df.columns.values)
df.to_csv('iidCheckAudit.csv',index=False)