import pandas as pd
import traceback
from os import walk
from fuzzywuzzy import fuzz


mypath = '/home/chinmay/Desktop/emailBot/auxFunctions/pullCorrect/data'
fileNames = []

for (dirpath, dirnames, filenames) in walk(mypath):
    fileNames.extend(filenames)
    break

exclude_sheets = ['Pivot', 'pivot']
edf = pd.DataFrame()
for f in fileNames:
    xlsx = pd.ExcelFile(mypath+'/'+f, engine='openpyxl')
    sheet_names = xlsx.sheet_names
    sheet_names = [i for i in sheet_names if i not in exclude_sheets]
    df = pd.read_excel(xlsx,sheet_names[0])
    print(df.columns.values)
    toCheck = ['Interaction','Sub query Type']
    dfMod = pd.DataFrame()
    dfMod['Interaction Id'] = ''
    dfMod['Sub query Type'] = ''
    for i in df.columns.values:
        for j in toCheck:
            # print(i)
            print(j)
            print(fuzz.ratio(i,j))
            if fuzz.ratio(i,j) >= 70:
                dfMod[] = df[i]
            #     # print(dfMod)
            # else:
            #     dfMod[j] = ''
    # print(dfMod)
    # edf.append(dfMod)

print(edf)