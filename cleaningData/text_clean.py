import re
from bs4 import BeautifulSoup
from pymongo import MongoClient
import traceback
import pandas as pd

client = MongoClient(host=['127.0.0.1:27017'],
                     document_class=dict,
                     tz_aware=False,
                     connect=True)

mongo_db = client["motilal_data_20"]


def run(doc):

    # h = html2text.HTML2Text()
    # h.ignore_links = True

    doc = str(doc)
    doc = re.sub("(?s)<style[>\s](.*?)</style>", " ", doc)
    soup = BeautifulSoup(doc)
    tag_re = re.compile(r"(<!--.*?-->|<[^>]*>)")
    # Remove well-formed tags, fixing mistakes by legitimate users
    doc = tag_re.sub("", soup.get_text())
    doc = str(doc)
    # doc = h.handle(str(doc))
    doc = doc.lower().strip()
    doc = re.sub(
        "subject:|date:|re:|fwd:|fw:|------ forwarded message ------|------ original message -------",
        " ",
        doc,
    )
    doc = doc.replace(
        """caution this email originated from outside of the organization .  do not click links or open attachments unless you recognize the sender and know the content is safe .""",
        "",
    )
    doc = doc.replace("\n", " ")
    doc = doc.replace("\\n", " ")
    doc = doc.replace("\\", " ")
    doc = doc.lower().strip()
    doc = doc.replace("policy id", "policy number")
    doc = doc.replace(":", "")
    doc = doc.replace("<", " ")
    doc = doc.replace(">", " ")
    doc = doc.replace("/n", " ")
    doc = doc.replace("fwd", " ")
    doc = doc.replace("_", " ")
    doc = re.sub(
        r"\\n", " ", doc, flags=re.IGNORECASE | re.MULTILINE
    )  # Next line character replaced by space
    doc = re.sub(r"\\r", " ", doc, flags=re.IGNORECASE | re.MULTILINE)
    doc = re.sub("re:", " ", doc)
    # doc = re.sub("re ", " ", doc)
    doc = re.sub("tps \d{3} \d{3} \d{3}", " ", doc)
    doc = re.sub("tps=\d{3}-\d{3}-\d{3}", " ", doc)
    doc = re.sub(
        "[!#$%^=?&*/-]", " ", doc
    )  # Special Characters present in body of email
    # print(doc,"1")
    doc = re.sub("\n", " ", doc)  # Next line character replaced by space
    doc = re.sub("dear team|dear all", " ", doc,)  # Greetings replaced
    doc = re.sub("hi team", " ", doc)  # Greetings replaced
    doc = re.sub("hello|hie|helloy|halo|hola|hie team",
                 " ", doc)  # Greetings replaced
    doc = re.sub("dear sir/madam|dear sir|hi ",
                 " ", doc,)  # Greetings replaced
    doc = re.sub(">", " ", doc)  # Special Character removal
    # End of email greetings removed
    doc = re.sub("(regards,*|regards.*)", " ", doc)
    doc = re.sub("disclaimer:-.*", " ", doc)  # End of email greetings removed
    doc = re.sub(
        "customer has put up a query through customer app.", " ", doc
    )  # End of email greetings removed
    # End of email greetings removed
    doc = re.sub("Details are as below", " ", doc)
    doc = re.sub(
        "kindly check and confirm", " ", doc
    )  # End of email greetings removed
    doc = re.sub("body", " ", doc)  # End of email greetings removed
    doc = re.sub(
        "[\n]|[\r]|subject:|thanks|thank|[^\\s]*@[^\\s]*|sent from.*.smartphone|sent from.*.phone|regards,.*|disclaimer.*|www.*.com|www.*.in|www.*.net|sent from.*.mobile",
        " ",
        doc,
    )  # End of email greetings removed
    # End of email greetings removed
    # print(doc,"2")

    doc = re.sub(
        "margin 0cm 0cm 0pt|margin 0cm 0cm 0pt div|imprintuniqueid|{|}", "", doc
    )
    doc = re.sub("margin 0cm 0cm 0pt", "", doc)
    doc = re.sub("0cm 0cm 0pt", "", doc)
    doc = re.sub("n\\.o\\.c", "noc", doc)  # Shortforms corrected
    doc = re.sub("(a\\.c|a\\.c\\.|a\\/c)", "account",
                 doc)  # Shortforms corrected
    doc = re.sub("(r\\.c|r\\.c\\.)", "rc", doc)  # Shortforms corrected
    doc = re.sub("e\\.m\\.i", "emi", doc)  # Shortforms corrected
    doc = re.sub("e\\.c\\.s", "ecs", doc)  # Shortforms corrected
    doc = re.sub("t\\.d\\.s", "tds", doc)  # Shortforms corrected
    doc = re.sub("l\\.o\\.d", "lod", doc)  # Shortforms corrected
    doc = re.sub("i\\.r\\.o", "iro", doc)  # Shortforms corrected
    doc = re.sub("chq", "cheque", doc)  # Shortforms corrected
    doc = re.sub(r"amt\.|amt", "amount ", doc)  # Shortforms corrected
    doc = re.sub(
        r"\srs\s?\.?| RS\s?\.? | Rs\s?\.?", " rupees ", doc
    )  # Shortforms corrected
    doc = re.sub(r"\s\s+", " ", doc)
    # Multiple fullstops replaced by single.
    doc = re.sub(
        "caution this email originated from outside of the organization", " ", doc
    )
    doc = re.sub(
        "do not click links or open attachments unless you recognize the sender and know the content is safe",
        " ",
        doc,
    )
    doc = re.sub(" re |fw ", " ", doc)
    doc = re.sub(r"\.+", " . ", doc)
    return doc

print('we are here')
mongo_cl = mongo_db['whole_data']
countDone = 0
countDoneIidList = []

countNotDone = 0
countNotDoneIidList = []

for count, i in enumerate(mongo_cl.find({})):
    print(count)
    try:
        #cursor_data = mongo_db.clean_data.insert({"Query": run(i['tSubject'] +
                                                                #i["mMsgContent"]), "Category": i["MasterDepartment"] + "||" + i["Department"] + "||" + i["QueryType"] + "||" + i["SubQueryType"]})
        checkCategory = i["MasterDepartment"] + "||" + i["Department"] + "||" + i["QueryType"] + "||" + i["SubQueryType"]
        countDoneIidList.append(i['nGlobalCaseID'])
        countDone+=1
    except:
        # traceback.print_exc()
        countNotDoneIidList.append(i['nGlobalCaseID'])
        countNotDone+=1

print(f'Count done:{countDone}')
print(f'Count not done:{countNotDone}')

df = pd.DataFrame({'Done':pd.Series(countDoneIidList).astype(int),'Not Done':pd.Series(countNotDoneIidList).astype(int)})
df['Not Done'].fillna(0,inplace=True)
df['Not Done'] = df['Not Done'].astype(int)
df.to_csv('NoneIidList.csv')